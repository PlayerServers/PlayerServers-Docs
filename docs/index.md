# PlayerServers overview

## Description

PlayerServers is an advanced plugin that will allow your players to create their own BungeeCord sub-server with just one command. They'll have all permissions and will be able to manage them with an easy-to-use in-game GUI in order to manage plugins and configuration files. Optionally, you can extend it's functionality by configuring Docker or deploying servers directly on Pterodactyl panel.

## Features

* **Async, lag-free:** The plugin is running asynchronously making use of all cores of your machine. Servers are created in seconds with no performance impact on your network.
* **Templates:** Make the game for your players more fun by giving them already-built templates to choose from. Servers will be initialised with predefined plugins, worlds, mods and configuration files - all at your will.
* **HubCore:** HubCore is an addon to PlayerServers which adds nice features to your lobby such as PlayerServers selector, option to create and manage servers with an intuitive GUI, options to disable some of the basic Minecraft functionalities such as rain or damage and much more - and the best part - It's completely free and opensource.
* **Automatically terminate inactive servers**: if servers are inactive (or don't have any players) for a configurable amount of time, they will automatically shutdown.
* **ControlGUI:** Once your players enter your server, they'll be presented with instructions on how to proceed and do basic tasks, the most common of which is managing their server with PlayerServer GUI. From this interface, they'll be able to install plugins, edit server variables, edit configuration files and much more!
* **Permissions-based:** In PlayerServers everything's configurable through config files, or dynamically through Permissions. Limit the amount of plugins your players may have, max players, RAM or CPU and monetise your server by giving better perks to donators that support your work.
* **Automatic & works out of the box:** The plugin is working out of the box without any need for external dependencies. Servers are added and removed from your network seamlessly without any need for reboot or greload.

## Purchase it now

{% embed url="https://builtbybit.com/resources/15521/" %}
Purchase on BuiltByBit (MC-Market)
{% endembed %}

{% embed url="https://www.spigotmc.org/resources/82268/" %}
Purchase on SpigotMC
{% endembed %}

{% embed url="https://polymart.org/resource/playerservers.57" %}
Purchase on Polymart
{% endembed %}

## 📘 Overview

* [🖥 PlayerServers - Overview](index)
* [🚀 Plugin Installation Tutorial](overview/installation/README.md)
  * [🛳 Adding a node](overview/installation/adding-a-node.md)
  * [🎁 Templates](overview/installation/templates.md)
  * [📦 Adding pre-defined plugins](overview/installation/adding-pre-defined-plugins.md)
* [🌐 Pterodactyl configuration](overview/installation/pterodactyl-configuration.md)
  * [🕊 Download custom wings](overview/pterodactyl-configuration/download-custom-wings.md)
  * [🔓 How to create tokens](overview/pterodactyl-configuration/how-to-create-tokens.md)
  * [⛲ Setting up plugins mount](overview/pterodactyl-configuration/setting-up-plugins-mount.md)
* [📜 Current Config files](overview/config.md)
* [📄 Commands & permissions](overview/permissions-and-commands.md)
* [🚫 Limitations](overview/limitations/README.md)
  * [Permission-based RAM](overview/limitations/permission-based-ram.md)
  * [Permission-based CPU](overview/limitations/permission-based-ram-1.md)
  * [Max players limitation](overview/limitations/max-players-limitation.md)
  * [Max plugins limitation](overview/limitations/max-plugins-limitation.md)

***

* [👵 Legacy (v2.x)](legacy/README.md)
  * [Updates](legacy/updates/README.md)
    * [PlayerServers - General](legacy/updates/master.md)
    * [v1.1 to v1.2](legacy/updates/v1.1-to-v1.2.md)
    * [8.0 to v1.0](legacy/updates/8.0-to-v1.0.md)
    * [5.0 to 6.0](legacy/updates/5.0-to-6.0.md)
    * [3.7-D to 4.0](legacy/updates/update1.md)
  * [Basic instructions](legacy/basic-instructions.md)
  * [Plugin management](legacy/plugin-management.md)
  * [Plugin Installation Tutorial](legacy/installation.md)
  * [Editing config files](legacy/editing-config-files.md)
  * [Current Config & Messages File](legacy/config.md)
  * [Permissions & Commands](legacy/permissions-and-commands.md)
  * [Limitations](legacy/limitations/README.md)
    * [Permission-based RAM](legacy/limitations/permission-based-ram.md)
    * [Max players limitation](legacy/limitations/max-players-limitation.md)
    * [Max plugins limitation](legacy/limitations/max-plugins-limitation.md)
  * [Adding pre-defined plugins](legacy/adding-pre-defined-plugins.md)
  * [Creating MySQL database](legacy/creating-mysql-database.md)
  * [Additional Tutorials (FAQ)](legacy/additional-tutorials-faq.md)
  * [Screen (accessing consoles)](legacy/screen.md)
  * [Screen (advanced)](https://linuxize.com/post/how-to-use-linux-screen/)
  * [Templates](legacy/templates.md)
  * [Multi-Node Support](legacy/multi-node-support.md)
  * [Unofficial Video Tutorial](https://www.youtube.com/watch?v=VApcwAG4y5c)
  * [MC-Market Thread](https://www.mc-market.org/threads/583445/#post-4508558)

## 🛍 Buy

* [🏪 BuiltByBit](https://builtbybit.com/resources/15521/)
* [🚿 SpigotMC](https://www.spigotmc.org/resources/82268)

***

* [🔗 Support Discord](https://discord.io/arcadiaservices)
